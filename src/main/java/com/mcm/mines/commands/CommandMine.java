package com.mcm.mines.commands;

import com.mcm.core.Main;
import com.mcm.core.database.MinesDb;
import com.mcm.core.database.TagDb;
import com.mcm.mines.hashs.isSelecting;
import com.mcm.mines.hashs.creatingMine;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;

public class CommandMine implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();

        String[] perm = {"superior"};
        if (command.getName().equalsIgnoreCase("mine") || command.getName().equalsIgnoreCase("mina")) {
            if (Arrays.asList(perm).contains(TagDb.getTag(uuid))) {
                if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("create") || args[0].equalsIgnoreCase("criar")) {
                        if (isSelecting.get(uuid) == null) {
                            new isSelecting(uuid).insert();
                            player.sendMessage(Main.getTradution("3Uk?ZPRbPeNzDt7", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else {
                            player.sendMessage(Main.getTradution("paejsrHqUe&4NT!", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else if (args[0].equalsIgnoreCase("set") || args[0].equalsIgnoreCase("setar")) {
                        if (isSelecting.get(uuid) != null) {
                            creatingMine.get(uuid).addSpawn(player.getWorld().getName() + "," + player.getLocation().getBlockX() + "," + player.getLocation().getBlockY() + "," + player.getLocation().getBlockZ() + "," + player.getLocation().getYaw() + "," + player.getLocation().getPitch());
                            player.sendMessage(Main.getTradution("D7rQPw%UBaR!2Se", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else {
                            player.sendMessage(Main.getTradution("cFw&S4ezZEz57*R", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else if (args[0].equalsIgnoreCase("finish") || args[0].equalsIgnoreCase("finalizar")) {
                        if (creatingMine.get(uuid) != null && !creatingMine.get(uuid).getPos1().isEmpty() && !creatingMine.get(uuid).getPos2().isEmpty() && creatingMine.get(uuid).getOres() != null && creatingMine.get(uuid).getSpawns() != null && !creatingMine.get(uuid).getName().isEmpty() && creatingMine.get(uuid).getOres().size() >= 1 && creatingMine.get(uuid).getSpawns().size() >= 1) {
                            String[] pos1 = creatingMine.get(uuid).getPos1().split(":");
                            String[] pos2 = creatingMine.get(uuid).getPos2().split(":");
                            Location location1 = new Location(Bukkit.getWorld(pos1[0]), Integer.valueOf(pos1[1]), Integer.valueOf(pos1[2]), Integer.valueOf(pos1[3]));
                            Location location2 = new Location(Bukkit.getWorld(pos2[0]), Integer.valueOf(pos2[1]), Integer.valueOf(pos2[2]), Integer.valueOf(pos2[3]));

                            int minX = location1.getBlockX() < location2.getBlockX() ? location1.getBlockX() : location2.getBlockX();
                            int maxX = location1.getBlockX() > location2.getBlockX() ? location1.getBlockX() : location2.getBlockX();
                            int minY = location1.getBlockY() < location2.getBlockY() ? location1.getBlockY() : location2.getBlockY();
                            int maxY = location1.getBlockY() > location2.getBlockY() ? location1.getBlockY() : location2.getBlockY();
                            int minZ = location1.getBlockZ() < location2.getBlockZ() ? location1.getBlockZ() : location2.getBlockZ();
                            int maxZ = location1.getBlockZ() > location2.getBlockZ() ? location1.getBlockZ() : location2.getBlockZ();

                            String blocks = null;
                            for (int y = minY; y <= maxY; y++) {
                                for (int x = minX; x <= maxX; x++) {
                                    for (int z = minZ; z <= maxZ; z++) {
                                        Location location = new Location(Bukkit.getWorld(pos1[0]), x, y, z);
                                        if (location.getBlock() != null && location.getBlock().getType().equals(Material.WET_SPONGE)) {
                                            if (blocks == null) blocks = location.getWorld().getName() + "," + x + "," + y + "," + z;
                                            else blocks += ":" + location.getWorld().getName() + "," + x + "," + y + "," + z;
                                        }
                                    }
                                }
                            }

                            MinesDb.createMine(creatingMine.get(uuid).getName(), creatingMine.get(uuid).getPos1(), creatingMine.get(uuid).getPos2(), blocks, creatingMine.get(uuid).getSpawns(), creatingMine.get(uuid).getOres());
                            player.sendMessage(Main.getTradution("!5cZzfBRk8Wp@$P", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                            creatingMine.get(uuid).delete();
                            isSelecting.get(uuid).delete();
                        } else {
                            player.sendMessage(Main.getTradution("N#a6xmuEF*cg8%f", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else errorMsg(player);
                } else if (args.length == 2) {
                    if (args[0].equalsIgnoreCase("ores") || args[0].equalsIgnoreCase("minerios")) {
                        if (isSelecting.get(uuid) != null) {
                            ArrayList<Material> ores = new ArrayList<>();
                            if (args[1].contains(",")) {
                                String[] split = args[1].split(",");
                                for (String m : split) {
                                    if (Material.valueOf(m) != null) ores.add(Material.valueOf(m));
                                }
                            } else if (Material.valueOf(args[1]) != null) ores.add(Material.valueOf(args[1]));

                            if (ores.isEmpty()) {
                                player.sendMessage(Main.getTradution("9G?S3XeG?%C9*pC", uuid));
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                return false;
                            }

                            for (Material o : ores) creatingMine.get(uuid).addOre(o);
                            player.sendMessage(Main.getTradution("5Nh55syF!DBTBR#", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else {
                            player.sendMessage(Main.getTradution("cFw&S4ezZEz57*R", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else if (args[0].equalsIgnoreCase("setname") || args[0].equalsIgnoreCase("setarnome")) {
                        if (isSelecting.get(uuid) != null) {
                            creatingMine.get(uuid).setName(args[1]);

                            player.sendMessage(Main.getTradution("!8nyYCyt$&8nH8r", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else {
                            player.sendMessage(Main.getTradution("cFw&S4ezZEz57*R", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else errorMsg(player);
                } else errorMsg(player);
            }
        }
        return false;
    }

    private static void errorMsg(Player player) {
        player.sendMessage(Main.getTradution("!Vd6$!t6Vt%Xzkf", player.getUniqueId().toString()));
        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
    }
}
