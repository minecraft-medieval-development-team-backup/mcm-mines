package com.mcm.mines.managers;

import com.mcm.core.Main;
import com.mcm.core.database.MinesDb;
import com.mcm.core.utils.EnchantmentGlow;
import com.mcm.core.utils.RemoveAllFlags;
import com.mcm.core.utils.TimeUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

public class InventoryNpcMiner {

    public static final DecimalFormat formatter = new DecimalFormat("#,###.00");

    public static int ironPickaxe = 4500;
    public static int goldPickaxe = 3500;
    public static int diamondPickaxe = 15000;

    public static Inventory get(String uuid, int mina) {
        Inventory inventory = Bukkit.createInventory(null, 3 * 9, Main.getTradution("a2PHfa2cP6?pt4%", uuid));

        ItemStack exchanges = new ItemStack(Material.MAGENTA_GLAZED_TERRACOTTA);
        ItemMeta metaExchanges = exchanges.getItemMeta();
        metaExchanges.setDisplayName(Main.getTradution("SQKGg978pk%pY#&", uuid));
        exchanges.setItemMeta(metaExchanges);

        ItemStack shop = new ItemStack(Material.GOLD_INGOT);
        ItemMeta metaShop = shop.getItemMeta();
        metaShop.setDisplayName(Main.getTradution("uZQJ5zugDB22j*e", uuid));
        shop.setItemMeta(metaShop);

        ItemStack mine = new ItemStack(Material.STONE_PICKAXE);
        ItemMeta metaMine = mine.getItemMeta();
        metaMine.setDisplayName(Main.getTradution("PU?26Py6E9Q8fqy", uuid));
        ArrayList<String> lore = new ArrayList<>();
        if (MinesDb.getTime(uuid, mina) <= 0) {
            lore.add(Main.getTradution("De4AP5R$R!jhb6&", uuid) + Main.getTradution("p3gBj8%w#XWxyxR", uuid));
        } else if ((MinesDb.getTime(uuid, mina) / 1000) - (new Date().getTime() / 1000) <= 0) {
            lore.add(Main.getTradution("De4AP5R$R!jhb6&", uuid) + Main.getTradution("p3gBj8%w#XWxyxR", uuid));
        } else lore.add(Main.getTradution("De4AP5R$R!jhb6&", uuid) + ChatColor.RED + TimeUtil.getTime(((MinesDb.getTime(uuid, mina) / 1000) - (new Date().getTime()) / 1000)));
        lore.add(" ");
        lore.add(Main.getTradution("qw!wK7BDYBSK&7b", uuid));
        metaMine.setLore(lore);
        mine.setItemMeta(metaMine);

        inventory.setItem(11, exchanges);
        inventory.setItem(12, shop);
        if (MinesDb.getTime(uuid, mina) == 0) inventory.setItem(15, EnchantmentGlow.addGlow(RemoveAllFlags.remove(mine)));
        if (MinesDb.getTime(uuid, mina) != 0 && ((MinesDb.getTime(uuid, mina) / 1000) - (new Date().getTime() / 1000)) <= 0) inventory.setItem(15, EnchantmentGlow.addGlow(RemoveAllFlags.remove(mine)));
        else inventory.setItem(15, RemoveAllFlags.remove(mine));
        return inventory;
    }

    public static Inventory getShop(String uuid) {
        Inventory inventory = Bukkit.createInventory(null, 3 * 9, Main.getTradution("DwU7Rm*FJaY5C3!", uuid));

        ItemStack woodenPick = new ItemStack(Material.WOODEN_PICKAXE);
        ItemMeta metaWPick = woodenPick.getItemMeta();
        metaWPick.setDisplayName(Main.getTradution("$BMjreu*7Kxff#V", uuid));
        ArrayList<String> loreWPick = new ArrayList<>();
        loreWPick.add(Main.getTradution("5pVvACw9T%FYKda", uuid) + Main.getTradution("gMS@nPm@Qw$S4JV", uuid));
        loreWPick.add(" ");
        loreWPick.add(Main.getTradution("Ng?uV!d7hJynxSq", uuid));
        metaWPick.setLore(loreWPick);
        woodenPick.setItemMeta(metaWPick);

        ItemStack ironPick = new ItemStack(Material.IRON_PICKAXE);
        ItemMeta metaIPick = ironPick.getItemMeta();
        metaIPick.setDisplayName(Main.getTradution("DrJxN&fbN56j*vS", uuid));
        ArrayList<String> loreIPick = new ArrayList<>();
        loreIPick.add(Main.getTradution("5pVvACw9T%FYKda", uuid) + ChatColor.GREEN + "$" + formatter.format(ironPickaxe));
        loreIPick.add(" ");
        loreIPick.add(Main.getTradution("Ng?uV!d7hJynxSq", uuid));
        metaIPick.setLore(loreIPick);
        ironPick.setItemMeta(metaIPick);

        ItemStack goldPick = new ItemStack(Material.GOLDEN_PICKAXE);
        ItemMeta metaGPick = goldPick.getItemMeta();
        metaGPick.setDisplayName(Main.getTradution("MWk3XgPHDT8g5q!", uuid));
        ArrayList<String> loreGPick = new ArrayList<>();
        loreGPick.add(Main.getTradution("5pVvACw9T%FYKda", uuid) + ChatColor.GREEN + "$" + formatter.format(goldPickaxe));
        loreGPick.add(" ");
        loreGPick.add(Main.getTradution("Ng?uV!d7hJynxSq", uuid));
        metaGPick.setLore(loreGPick);
        goldPick.setItemMeta(metaGPick);

        ItemStack diamondPick = new ItemStack(Material.DIAMOND_PICKAXE);
        ItemMeta metaDPick = diamondPick.getItemMeta();
        metaDPick.setDisplayName(Main.getTradution("GRQq5Qhxa#ZESfe", uuid));
        ArrayList<String> loreDPick = new ArrayList<>();
        loreDPick.add(Main.getTradution("5pVvACw9T%FYKda", uuid) + ChatColor.GREEN + "$" + formatter.format(diamondPickaxe));
        loreDPick.add(" ");
        loreDPick.add(Main.getTradution("Ng?uV!d7hJynxSq", uuid));
        metaDPick.setLore(loreDPick);
        diamondPick.setItemMeta(metaDPick);

        ItemStack back = new ItemStack(Material.OAK_SIGN);
        ItemMeta metaBack = back.getItemMeta();
        metaBack.setDisplayName(Main.getTradution("NF@p4kK&Pk5Z$V%", uuid));
        ArrayList<String> loreBack = new ArrayList<>();
        loreBack.add(" ");
        loreBack.add(Main.getTradution("Zzh5y3?7EFzR3Ff", uuid));
        metaBack.setLore(loreBack);
        back.setItemMeta(metaBack);

        inventory.setItem(10, woodenPick);
        inventory.setItem(12, ironPick);
        inventory.setItem(14, goldPick);
        inventory.setItem(16, diamondPick);
        inventory.setItem(18, back);
        return inventory;
    }

    public static Inventory getExchanges(String uuid) {
        Inventory inventory = Bukkit.createInventory(null, 3 * 9, Main.getTradution("7?&GAuQ5%u59a9T", uuid));

        ItemStack unbreaking1 = new ItemStack(Material.ENCHANTED_BOOK);
        EnchantmentStorageMeta meta = (EnchantmentStorageMeta)unbreaking1.getItemMeta();
        meta.addEnchant(Enchantment.DURABILITY, 1, false);
        ArrayList<String> loreUnbk1 = new ArrayList<>();
        loreUnbk1.add(" ");
        loreUnbk1.add(Main.getTradution("5pVvACw9T%FYKda", uuid) + ChatColor.GREEN + formatter.format(1000) + Main.getTradution("KQrv3WFcT4FU*u*", uuid));
        loreUnbk1.add(" ");
        loreUnbk1.add(Main.getTradution("Ng?uV!d7hJynxSq", uuid));
        meta.setLore(loreUnbk1);
        unbreaking1.setItemMeta(meta);

        ItemStack unbreaking2 = new ItemStack(Material.ENCHANTED_BOOK);
        EnchantmentStorageMeta metaUnbk2 = (EnchantmentStorageMeta)unbreaking2.getItemMeta();
        metaUnbk2.addEnchant(Enchantment.DURABILITY, 2, false);
        ArrayList<String> loreUnbk2 = new ArrayList<>();
        loreUnbk2.add(" ");
        loreUnbk2.add(Main.getTradution("5pVvACw9T%FYKda", uuid) + ChatColor.GREEN + formatter.format(2000) + Main.getTradution("KQrv3WFcT4FU*u*", uuid));
        loreUnbk2.add(" ");
        loreUnbk2.add(Main.getTradution("Ng?uV!d7hJynxSq", uuid));
        metaUnbk2.setLore(loreUnbk2);
        unbreaking2.setItemMeta(metaUnbk2);

        ItemStack efficiency2 = new ItemStack(Material.ENCHANTED_BOOK);
        EnchantmentStorageMeta metaEff2 = (EnchantmentStorageMeta)efficiency2.getItemMeta();
        metaEff2.addEnchant(Enchantment.DIG_SPEED, 2, false);
        ArrayList<String> loreEff2 = new ArrayList<>();
        loreEff2.add(" ");
        loreEff2.add(Main.getTradution("5pVvACw9T%FYKda", uuid) + ChatColor.GREEN + formatter.format(3000) + Main.getTradution("KQrv3WFcT4FU*u*", uuid));
        loreEff2.add(" ");
        loreEff2.add(Main.getTradution("Ng?uV!d7hJynxSq", uuid));
        metaEff2.setLore(loreEff2);
        efficiency2.setItemMeta(metaEff2);

        ItemStack efficiency3 = new ItemStack(Material.ENCHANTED_BOOK);
        EnchantmentStorageMeta metaEff3 = (EnchantmentStorageMeta)efficiency3.getItemMeta();
        metaEff3.addEnchant(Enchantment.DIG_SPEED, 2, false);
        ArrayList<String> loreEff3 = new ArrayList<>();
        loreEff3.add(" ");
        loreEff3.add(Main.getTradution("5pVvACw9T%FYKda", uuid) + ChatColor.GREEN + formatter.format(5000) + Main.getTradution("KQrv3WFcT4FU*u*", uuid));
        loreEff3.add(" ");
        loreEff3.add(Main.getTradution("Ng?uV!d7hJynxSq", uuid));
        metaEff3.setLore(loreEff3);
        efficiency3.setItemMeta(metaEff3);

        ItemStack silktouch = new ItemStack(Material.ENCHANTED_BOOK);
        EnchantmentStorageMeta metaSilk = (EnchantmentStorageMeta)silktouch.getItemMeta();
        metaSilk.addEnchant(Enchantment.SILK_TOUCH, 1, false);
        ArrayList<String> loreSilk = new ArrayList<>();
        loreSilk.add(" ");
        loreSilk.add(Main.getTradution("5pVvACw9T%FYKda", uuid) + ChatColor.GREEN + formatter.format(8500) + Main.getTradution("KQrv3WFcT4FU*u*", uuid));
        loreSilk.add(" ");
        loreSilk.add(Main.getTradution("Ng?uV!d7hJynxSq", uuid));
        metaSilk.setLore(loreSilk);
        silktouch.setItemMeta(metaSilk);

        ItemStack fortune1 = new ItemStack(Material.ENCHANTED_BOOK);
        EnchantmentStorageMeta metaFort1 = (EnchantmentStorageMeta)fortune1.getItemMeta();
        metaFort1.addEnchant(Enchantment.LOOT_BONUS_BLOCKS, 1, false);
        ArrayList<String> loreFort1 = new ArrayList<>();
        loreFort1.add(" ");
        loreFort1.add(Main.getTradution("5pVvACw9T%FYKda", uuid) + ChatColor.GREEN + formatter.format(10000) + Main.getTradution("KQrv3WFcT4FU*u*", uuid));
        loreFort1.add(" ");
        loreFort1.add(Main.getTradution("Ng?uV!d7hJynxSq", uuid));
        metaFort1.setLore(loreFort1);
        fortune1.setItemMeta(metaFort1);

        ItemStack mending = new ItemStack(Material.ENCHANTED_BOOK);
        EnchantmentStorageMeta metaMending = (EnchantmentStorageMeta)mending.getItemMeta();
        metaMending.addEnchant(Enchantment.MENDING, 1, false);
        ArrayList<String> loreMending = new ArrayList<>();
        loreMending.add(" ");
        loreMending.add(Main.getTradution("5pVvACw9T%FYKda", uuid) + ChatColor.GREEN + formatter.format(13500) + Main.getTradution("KQrv3WFcT4FU*u*", uuid));
        loreMending.add(" ");
        loreMending.add(Main.getTradution("Ng?uV!d7hJynxSq", uuid));
        metaMending.setLore(loreMending);
        mending.setItemMeta(metaMending);

        ItemStack back = new ItemStack(Material.OAK_SIGN);
        ItemMeta metaBack = back.getItemMeta();
        metaBack.setDisplayName(Main.getTradution("NF@p4kK&Pk5Z$V%", uuid));
        ArrayList<String> loreBack = new ArrayList<>();
        loreBack.add(" ");
        loreBack.add(Main.getTradution("Zzh5y3?7EFzR3Ff", uuid));
        metaBack.setLore(loreBack);
        back.setItemMeta(metaBack);

        inventory.setItem(10, unbreaking1);
        inventory.setItem(11, unbreaking2);
        inventory.setItem(12, efficiency2);
        inventory.setItem(13, efficiency3);
        inventory.setItem(14, silktouch);
        inventory.setItem(15, fortune1);
        inventory.setItem(16, mending);
        inventory.setItem(18, back);
        return inventory;
    }
}
