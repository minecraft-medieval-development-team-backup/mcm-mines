package com.mcm.mines;

import com.mcm.mines.inventoryListeners.InventoryNpcMiner;
import com.mcm.mines.listeners.*;
import net.citizensnpcs.api.CitizensAPI;
import org.bukkit.Bukkit;

public class RegisterListeners {

    public static void register() {
        CitizensAPI.registerEvents(new InteractNpcEvent());
        Bukkit.getPluginManager().registerEvents(new SelectMineRegion(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerSuffocateEvent(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new BlockBreakEvent(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new InventoryNpcMiner(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerEntryOnMineEvent(), Main.plugin);
    }
}
