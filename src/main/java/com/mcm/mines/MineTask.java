package com.mcm.mines;

import com.mcm.core.Main;
import com.mcm.core.cache.MiningTime;
import com.mcm.core.cache.PlayerMine;
import com.mcm.core.database.MinesDb;
import com.mcm.mines.hashs.inMine;
import com.mcm.scoreboard.utils.ScoreboardManager;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

public class MineTask {

    private static int stonePorcent = 75;

    public static void runResetMine() {
        final Thread thread = new Thread(() -> {
            while (true) {
                try {
                    Thread.currentThread().sleep(60000 * 2); //5 Minutos
                } catch (final Exception e) {
                    e.printStackTrace();
                }

                for (int id: MinesDb.getIds()) {
                    ArrayList<Location> blocks = (ArrayList<Location>) MinesDb.getBlocks(id).clone();
                    int stoneQuantity = (blocks.size() / 100) * stonePorcent;

                    Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                        for (int i = 1; i <= stoneQuantity; i++) {
                            int random = new Random().nextInt(blocks.size() - 1);
                            if (blocks.get(random).getBlock().getLocation().getBlock().getType().equals(Material.AIR)) blocks.get(random).getBlock().getLocation().getBlock().setType(Material.STONE);
                            blocks.remove(random);
                        }

                        ArrayList<Material> ores = MinesDb.getOres(id);
                        for (Location loc : blocks) {
                            if (loc.getBlock().getLocation().getBlock().getType().equals(Material.AIR)) loc.getBlock().getLocation().getBlock().setType(ores.get(new Random().nextInt(ores.size())));
                        }
                    }, 5L);
                }
            }
        });
        thread.setName("mines.reset-thread");
        thread.start();
    }

    public static void runUpdateTime() {
        final Thread thread = new Thread(() -> {
            while (true) {
                try {
                    Thread.currentThread().sleep(60000 / 60); //1 segundos
                } catch (final Exception e) {
                    e.printStackTrace();
                }

                if (MiningTime.players != null) {
                    for (Player player : MiningTime.players) {
                        if (player != null && player.isOnline()) {
                            try {
                                if (MiningTime.get(player.getUniqueId().toString()).getTime() < 0) {
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                    player.sendMessage(Main.getTradution("mh8Fgwza&maxNqm", player.getUniqueId().toString()));
                                    MiningTime.get(player.getUniqueId().toString()).delete();
                                    return;
                                }
                                ScoreboardManager.updateTempo(player, inMine.get(player.getUniqueId().toString()).getMine());
                                MiningTime.get(player.getUniqueId().toString()).setTime(MiningTime.get(player.getUniqueId().toString()).getTime() - 1);
                            } catch (Exception e) {
                                //-
                            }
                        }
                    }
                }
            }
        });
        thread.setName("update.timers-thread");
        thread.start();
    }
}
