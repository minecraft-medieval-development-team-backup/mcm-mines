package com.mcm.mines.listeners;

import com.mcm.core.Main;
import com.mcm.mines.hashs.isSelecting;
import com.mcm.mines.hashs.creatingMine;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

public class SelectMineRegion implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        if (isSelecting.get(uuid) != null) {
            if (event.getAction().name().equals("RIGHT_CLICK_BLOCK")) {
                event.setCancelled(true);
                if (creatingMine.get(uuid) == null) {
                    new creatingMine(uuid).insert().setPos1(event.getClickedBlock().getWorld().getName() + ":" + event.getClickedBlock().getLocation().getBlockX() + ":" + event.getClickedBlock().getLocation().getBlockY() + ":" + event.getClickedBlock().getLocation().getBlockZ());
                } else creatingMine.get(uuid).setPos1(event.getClickedBlock().getWorld().getName() + ":" + event.getClickedBlock().getLocation().getBlockX() + ":" + event.getClickedBlock().getLocation().getBlockY() + ":" + event.getClickedBlock().getLocation().getBlockZ());

                player.sendMessage(Main.getTradution("#q$ZUGp&jC7Gtq2", uuid));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            } else if (event.getAction().name().equals("LEFT_CLICK_BLOCK")) {
                event.setCancelled(true);
                if (creatingMine.get(uuid) == null) {
                    new creatingMine(uuid).insert().setPos2(event.getClickedBlock().getWorld().getName() + ":" + event.getClickedBlock().getLocation().getBlockX() + ":" + event.getClickedBlock().getLocation().getBlockY() + ":" + event.getClickedBlock().getLocation().getBlockZ());
                } else creatingMine.get(uuid).setPos2(event.getClickedBlock().getWorld().getName() + ":" + event.getClickedBlock().getLocation().getBlockX() + ":" + event.getClickedBlock().getLocation().getBlockY() + ":" + event.getClickedBlock().getLocation().getBlockZ());

                player.sendMessage(Main.getTradution("c!W#3wMFT%hFg3x", uuid));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
    }
}
