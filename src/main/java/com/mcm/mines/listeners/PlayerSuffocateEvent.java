package com.mcm.mines.listeners;

import com.mcm.core.database.MinesDb;
import com.mcm.core.utils.MinesUtil;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class PlayerSuffocateEvent implements Listener {

    @EventHandler (priority = EventPriority.LOWEST)
    public void isSuffocating(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player && event.getCause().equals(EntityDamageEvent.DamageCause.SUFFOCATION)) {
            Player player = (Player) event.getEntity();
            int mine = MinesUtil.isOnMine(player.getLocation());
            if (mine != 0) {
                Location moreNext = null;
                int biggerDistance = 10000;
                for (Location loc : MinesDb.getSpawns(mine)) {
                    if (biggerDistance > (int) player.getLocation().distance(loc)) {
                        moreNext = loc.clone();
                        biggerDistance = (int) player.getLocation().distance(loc);
                    }
                }

                if (moreNext != null) {
                    player.teleport(moreNext);
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 1.0f, 1.0f);
                }
            }
        }
    }
}
