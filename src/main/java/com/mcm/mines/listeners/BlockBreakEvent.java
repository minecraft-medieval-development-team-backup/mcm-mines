package com.mcm.mines.listeners;

import com.mcm.core.Main;
import com.mcm.core.cache.MiningTime;
import com.mcm.core.database.CoinsDb;
import com.mcm.core.database.MinesDb;
import com.mcm.core.utils.*;
import com.mcm.mines.hashs.oresPrices;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDropItemEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class BlockBreakEvent implements Listener {

    @EventHandler (priority = EventPriority.LOWEST)
    public void onBreak(org.bukkit.event.block.BlockBreakEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        /**
         * Here's the prices and any infos of the ores!
         */
        if (oresPrices.get("registry") == null) {
            new oresPrices("registry", 0, 0, 0).insert();
            new oresPrices(Material.COAL_ORE.name(), 10, 2, 25).insert();
            new oresPrices(Material.LAPIS_ORE.name(), 12, 2, 30).insert();
            new oresPrices(Material.IRON_ORE.name(), 15, 2, 35).insert();
            new oresPrices(Material.REDSTONE_ORE.name(), 16, 2, 37).insert();
            new oresPrices(Material.NETHER_QUARTZ_ORE.name(), 17, 3, 20).insert();
            new oresPrices(Material.GOLD_ORE.name(), 23, 3, 25).insert();
            new oresPrices(Material.DIAMOND_ORE.name(), 27, 3, 35).insert();
            new oresPrices(Material.EMERALD_ORE.name(), 35, 4, 25).insert();
        }

        int mine = MinesUtil.isOnMine(event.getBlock().getLocation());
        if (mine != 0) {
            if (MiningTime.get(uuid) != null && MiningTime.get(uuid).getTime() >= 1) {
                if (MinesDb.getBlocks(mine).contains(event.getBlock().getLocation())) {
                    event.setDropItems(false);

                    if (oresPrices.get(event.getBlock().getType().name()) != null) {
                        int price = oresPrices.get(event.getBlock().getType().name()).getPrice();
                        CoinsDb.updateCoins(uuid, CoinsDb.getCoins(uuid) + price);
                        if (ProbabilityUtil.draw(oresPrices.get(event.getBlock().getType().name()).getProbability())) {
                            MinesDb.updateXP(player, mine, MinesDb.getXP(uuid, mine) + oresPrices.get(event.getBlock().getType().name()).getXp());
                            player.sendActionBar(ChatColor.RED + "+ " + oresPrices.get(event.getBlock().getType().name()).getXp() + " XP");
                        }
                        if (ProbabilityUtil.draw(7)) {
                            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), fragment(uuid));
                        }
                    } else {
                        if (ProbabilityUtil.draw(25)) {
                            MinesDb.updateXP(player, mine, MinesDb.getXP(uuid, mine) + 1);
                            player.sendActionBar(ChatColor.RED + "+ 1 XP");
                        }
                        if (ProbabilityUtil.draw(2)) {
                            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), fragment(uuid));
                        }
                    }
                }
            } else event.setCancelled(true);
        }
    }

    private static ItemStack fragment(String uuid) {
        ItemStack itemStack = new ItemStack(Material.IRON_NUGGET);
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(Main.getTradution("XfpkJX*3J8pBkZ$", uuid));
        itemStack.setItemMeta(itemMeta);
        return EnchantmentGlow.addGlow(RemoveAllFlags.remove(itemStack));
    }

    /**
     * Trying block the action when the iron_nugget is a fragment, this's because the fragment can't to transform in iron bar
     * @param event
     */
    @EventHandler
    public void moveFrag(InventoryMoveItemEvent event) {
        if (event.getItem().getType().equals(Material.IRON_NUGGET) && NBTTagUtil.getStringData(event.getItem(), "fragmento") != null && event.getDestination().getType().equals(InventoryType.WORKBENCH)) {
            event.setCancelled(true);
        }
    }
}
