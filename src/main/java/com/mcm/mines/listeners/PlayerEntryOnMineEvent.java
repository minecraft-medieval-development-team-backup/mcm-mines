package com.mcm.mines.listeners;

import com.mcm.core.Main;
import com.mcm.core.database.MinesDb;
import com.mcm.core.utils.MinesUtil;
import com.mcm.mines.hashs.inMine;
import com.mcm.scoreboard.utils.ScoreboardManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerEntryOnMineEvent implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        int mine = MinesUtil.isOnMine(player.getLocation());
        if (mine != 0 && inMine.get(uuid) == null) {
            new inMine(uuid, mine).insert();
            ScoreboardManager.loadScoreboardMines(player, mine);
            player.sendTitle(Main.getTradution("*CuBZ?h?erv7xpV", uuid), ChatColor.GRAY + MinesDb.getName(mine), 10, 40, 10);
            playSound(player);
        } else if (mine != 0 && inMine.get(uuid) != null) {
            if (inMine.get(uuid).getMine() != mine) {
                inMine.get(uuid).setMine(mine);
                ScoreboardManager.updateMina(player, MinesDb.getName(mine));
                ScoreboardManager.updateTempo(player, mine);
                ScoreboardManager.updateAtual(player, mine);
                ScoreboardManager.updateProximo(player, mine);
                player.sendTitle(Main.getTradution("*CuBZ?h?erv7xpV", uuid), ChatColor.GRAY + MinesDb.getName(mine), 10, 40, 10);
                playSound(player);
            } else ScoreboardManager.updateTempo(player, mine);
        } else if (mine == 0 && inMine.get(uuid) != null) {
            inMine.get(uuid).delete();
            ScoreboardManager.reloadMainScoreboard(player);
        }
    }

    private static void playSound(Player player) {
        player.playSound(player.getLocation(), Sound.BLOCK_METAL_BREAK, 1.3f, 1.0f);
        Bukkit.getScheduler().runTaskLaterAsynchronously(Main.plugin, () -> {
            player.playSound(player.getLocation(), Sound.BLOCK_STONE_BREAK, 1.0f, 1.0f);
        }, 5L);
        Bukkit.getScheduler().runTaskLaterAsynchronously(Main.plugin, () -> {
            player.playSound(player.getLocation(), Sound.BLOCK_STONE_BREAK, 1.2f, 1.0f);
            player.playSound(player.getLocation(), Sound.BLOCK_STONE_BREAK, 1.0f, 1.0f);
        }, 20L);
        Bukkit.getScheduler().runTaskLaterAsynchronously(Main.plugin, () -> {
            player.playSound(player.getLocation(), Sound.BLOCK_STONE_BREAK, 1.4f, 1.0f);
            player.playSound(player.getLocation(), Sound.BLOCK_METAL_BREAK, 1.5f, 1.0f);
        }, 30L);
        Bukkit.getScheduler().runTaskLaterAsynchronously(Main.plugin, () -> {
            player.playSound(player.getLocation(), Sound.ENTITY_ITEM_BREAK, 1.1f, 1.0f);
        }, 35L);
        Bukkit.getScheduler().runTaskLaterAsynchronously(Main.plugin, () -> {
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_CELEBRATE, 1.1f, 1.0f);
        }, 40L);
    }
}
