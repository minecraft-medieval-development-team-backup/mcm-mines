package com.mcm.mines.listeners;

import com.mcm.core.utils.MinesUtil;
import com.mcm.mines.managers.InventoryNpcMiner;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class InteractNpcEvent implements Listener {

    @EventHandler
    public void onClick(NPCRightClickEvent event) {
        Player player = event.getClicker();
        String uuid = player.getUniqueId().toString();

        if (event.getNPC().getId() == 0) {
            player.openInventory(InventoryNpcMiner.get(uuid, MinesUtil.isOnMine(player.getLocation())));
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
    }
}
