package com.mcm.mines.hashs;

import org.bukkit.Material;

import java.util.ArrayList;
import java.util.HashMap;

public class creatingMine {

    private String uuid;
    private String name;
    private String pos1;
    private String pos2;
    private ArrayList<String> spawns;
    private ArrayList<Material> ores;
    private static HashMap<String, creatingMine> cache = new HashMap<>();

    public creatingMine(String uuid) {
        this.uuid = uuid;
        this.spawns = new ArrayList<>();
        this.ores = new ArrayList<>();
    }

    public creatingMine insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static creatingMine get(String uuid) {
        return cache.get(uuid);
    }

    public void delete() {
        this.cache.remove(this.uuid);
    }

    public String getPos1() {
        return pos1;
    }

    public void setPos1(String pos1) {
        this.pos1 = pos1;
    }

    public String getPos2() {
        return pos2;
    }

    public void setPos2(String pos2) {
        this.pos2 = pos2;
    }

    public ArrayList<String> getSpawns() {
        return spawns;
    }

    public void addSpawn(String spawn) {
        this.spawns.add(spawn);
    }

    public ArrayList<Material> getOres() {
        return ores;
    }

    public void addOre(Material ore) {
        this.ores.add(ore);
    }

    public void setOres(ArrayList<Material> ores) {
        this.ores = ores;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
