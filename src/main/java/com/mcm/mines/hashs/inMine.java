package com.mcm.mines.hashs;

import java.util.HashMap;

public class inMine {

    private String uuid;
    private int mine;
    private static HashMap<String, inMine> cache = new HashMap<>();

    public inMine(String uuid, int mine) {
        this.uuid = uuid;
        this.mine = mine;
    }

    public inMine insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static inMine get(String uuid) {
        return cache.get(uuid);
    }

    public void delete() {
        this.cache.remove(this.uuid);
    }

    public int getMine() {
        return mine;
    }

    public void setMine(int mine) {
        this.mine = mine;
    }
}
