package com.mcm.mines.hashs;

import java.util.HashMap;

public class woodenPickLimit {

    private String uuid;
    private long unlock;
    private static HashMap<String, woodenPickLimit> cache = new HashMap<>();

    public woodenPickLimit(String uuid, long unlock) {
        this.uuid = uuid;
        this.unlock = unlock;
    }

    public woodenPickLimit insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static woodenPickLimit get(String uuid) {
        return cache.get(uuid);
    }

    public long getUnlock() {
        return unlock;
    }

    public void setUnlock(long unlock) {
        this.unlock = unlock;
    }
}
