package com.mcm.mines.hashs;

import java.util.HashMap;

public class isSelecting {

    private String uuid;
    private static HashMap<String, isSelecting> cache = new HashMap<>();

    public isSelecting(String uuid) {
        this.uuid = uuid;
    }

    public isSelecting insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static isSelecting get(String uuid) {
        return cache.get(uuid);
    }

    public void delete() {
        cache.remove(this.uuid);
    }
}
