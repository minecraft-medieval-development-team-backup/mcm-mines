package com.mcm.mines.hashs;

import org.bukkit.Material;

import java.util.HashMap;

public class oresPrices {

    private String ore;
    private int price;
    private int xp;
    private int probability;
    private static HashMap<String, oresPrices> cache = new HashMap<>();

    public oresPrices(String ore, int price, int xp, int probability) {
        this.ore = ore;
        this.price = price;
        this.xp = xp;
        this.probability = probability;
    }

    public oresPrices insert() {
        this.cache.put(this.ore, this);
        return this;
    }

    public static oresPrices get(String ore) {
        return cache.get(ore);
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }

    public int getProbability() {
        return probability;
    }

    public void setProbability(int probability) {
        this.probability = probability;
    }
}
