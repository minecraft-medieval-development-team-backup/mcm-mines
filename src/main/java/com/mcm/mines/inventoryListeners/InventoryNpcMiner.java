package com.mcm.mines.inventoryListeners;

import com.mcm.core.cache.MiningTime;
import com.mcm.core.database.CoinsDb;
import com.mcm.core.database.MinesDb;
import com.mcm.core.utils.TimeUtil;
import com.mcm.mines.Main;
import com.mcm.mines.hashs.inMine;
import com.mcm.mines.hashs.woodenPickLimit;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.text.DecimalFormat;
import java.util.Date;

public class InventoryNpcMiner implements Listener {

    public static final DecimalFormat formatter = new DecimalFormat("#,###.00");

    @EventHandler
    public void onInteractInv(final InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();
        final String uuid = player.getUniqueId().toString();

        //exchanges
        if (event.getClickedInventory() != null && event.getView().getTitle().equalsIgnoreCase(com.mcm.core.Main.getTradution("7?&GAuQ5%u59a9T", uuid))) {
            event.setCancelled(true);

            if (event.getSlot() == 18) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                    player.openInventory(com.mcm.mines.managers.InventoryNpcMiner.get(uuid, inMine.get(uuid).getMine()));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }, 5L);
            }
        }

        //shop
        if (event.getClickedInventory() != null && event.getView().getTitle().equalsIgnoreCase(com.mcm.core.Main.getTradution("DwU7Rm*FJaY5C3!", uuid))) {
            event.setCancelled(true);
            if (event.getSlot() == 16) {
                sell(player, Material.DIAMOND_PICKAXE);
            }
            if (event.getSlot() == 14) {
                sell(player, Material.GOLDEN_PICKAXE);
            }
            if (event.getSlot() == 12) {
                sell(player, Material.IRON_PICKAXE);
            }
            if (event.getSlot() == 10) {
                sell(player, Material.WOODEN_PICKAXE);
            }
            if (event.getSlot() == 18) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                    player.openInventory(com.mcm.mines.managers.InventoryNpcMiner.get(uuid, inMine.get(uuid).getMine()));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }, 5L);
            }
        }

        //menu
        if (event.getClickedInventory() != null && event.getView().getTitle().equalsIgnoreCase(com.mcm.core.Main.getTradution("a2PHfa2cP6?pt4%", uuid))) {
            event.setCancelled(true);

            if (event.getSlot() == 15) {
                if (MinesDb.getTime(uuid, inMine.get(uuid).getMine()) <= 0) {
                    mining(uuid, player);
                } else if ((MinesDb.getTime(uuid, inMine.get(uuid).getMine()) / 1000) - (new Date().getTime() / 1000) <= 0) {
                    mining(uuid, player);
                } else {
                    player.sendMessage(com.mcm.core.Main.getTradution("XQxxdQM!bZCPK$5", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }

            if (event.getSlot() == 11) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                    player.openInventory(com.mcm.mines.managers.InventoryNpcMiner.getExchanges(uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }, 5L);
            }

            if (event.getSlot() == 12) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                    player.openInventory(com.mcm.mines.managers.InventoryNpcMiner.getShop(uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }, 5L);
            }
        }
    }

    private static void sell(Player player, Material pickaxe) {
        boolean haveSpace = false;
        for (int i = 0; i <= player.getInventory().getSize(); i++) {
            if (player.getInventory().getItem(i) != null && !player.getInventory().getItem(i).getType().equals(Material.AIR)) haveSpace = false; else haveSpace = true;
        }

        if (pickaxe.equals(Material.WOODEN_PICKAXE)) {
            if (woodenPickLimit.get(player.getUniqueId().toString()) == null) {
                sellWPickaxe(player, player.getUniqueId().toString());
            } else if (woodenPickLimit.get(player.getUniqueId().toString()) != null && ((woodenPickLimit.get(player.getUniqueId().toString()).getUnlock() / 1000) - (new Date().getTime() / 1000)) <= 0) {
                sellWPickaxe(player, player.getUniqueId().toString());
            } else {
                player.sendMessage(com.mcm.core.Main.getTradution("*nkqj8uE4$YDChk", player.getUniqueId().toString()) + TimeUtil.getTime(((woodenPickLimit.get(player.getUniqueId().toString()).getUnlock() / 1000) - (new Date().getTime() / 1000))));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            }
        }

        if (haveSpace) {
            int price = 0;
            if (pickaxe.equals(Material.IRON_PICKAXE)) price = com.mcm.mines.managers.InventoryNpcMiner.ironPickaxe; else if (pickaxe.equals(Material.GOLDEN_PICKAXE)) price = com.mcm.mines.managers.InventoryNpcMiner.goldPickaxe; else if (pickaxe.equals(Material.DIAMOND_PICKAXE)) price = com.mcm.mines.managers.InventoryNpcMiner.diamondPickaxe;

            if (CoinsDb.getCoins(player.getUniqueId().toString()) >= price) {
                player.getInventory().addItem(new ItemStack(pickaxe));
                player.updateInventory();
                player.sendMessage(com.mcm.core.Main.getTradution("?*HGUZ22%Mph9%k", player.getUniqueId().toString()) + formatter.format(price));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            } else {
                player.sendMessage(com.mcm.core.Main.getTradution("Uu3c#Xta3j!wTAy", player.getUniqueId().toString()));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            }
        } else {
            player.sendMessage(com.mcm.core.Main.getTradution("62pxkegj8&?Be6n", player.getUniqueId().toString()));
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
        }
    }

    private static void sellWPickaxe(Player player, String uuid) {
        boolean haveSpace = false;
        for (int i = 0; i <= player.getInventory().getSize(); i++) {
            if (player.getInventory().getItem(i) != null && !player.getInventory().getItem(i).getType().equals(Material.AIR)) haveSpace = false; else haveSpace = true;
        }

        if (haveSpace) {
            Date unlock = new Date();
            unlock.setMinutes(unlock.getMinutes() + 30);
            if (woodenPickLimit.get(uuid) != null) woodenPickLimit.get(uuid).setUnlock(unlock.getTime()); else new woodenPickLimit(uuid, unlock.getTime()).insert();

            player.getInventory().addItem(new ItemStack(Material.WOODEN_PICKAXE));
            player.updateInventory();
            player.sendMessage(com.mcm.core.Main.getTradution("vnpA5Ufrxd9!Tsb", uuid));
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        } else {
            player.sendMessage(com.mcm.core.Main.getTradution("62pxkegj8&?Be6n", uuid));
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
        }
    }

    private static void mining(String uuid, Player player) {
        new MiningTime(uuid, player, 300).insert();

        Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
            player.getOpenInventory().close();
            player.sendMessage(com.mcm.core.Main.getTradution("T33EKjHG%fq6?cK", uuid));
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.0f);

            Date time = new Date();
            time.setHours(time.getHours() + 2);
            MinesDb.updateTime(player.getUniqueId().toString(), inMine.get(player.getUniqueId().toString()).getMine(), time.getTime());
        }, 5L);
    }
}
