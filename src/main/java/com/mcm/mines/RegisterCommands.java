package com.mcm.mines;

import com.mcm.mines.commands.CommandMine;

public class RegisterCommands {

    public static void register() {
        Main.instance.getCommand("mina").setExecutor(new CommandMine());
        Main.instance.getCommand("mine").setExecutor(new CommandMine());
    }
}
