package com.mcm.mines;

import com.mcm.mines.hashs.oresPrices;

public class RegisterMain {

    public static void register() {
        MineTask.runResetMine();
        MineTask.runUpdateTime();
    }
}
